import React, { useEffect, useState } from 'react'
import { List } from '../components'
import { getList } from '../http'
import { sortArr } from '../utils/sortArr'
import { transformCodesToBirthdays } from '../utils/transformCodesToBirthdays'
import { divideArr } from '../utils/divideArr'
import { withRouter, useLocation } from 'react-router-dom'

function useQuery() {
  const { search } = useLocation()

  return React.useMemo(() => new URLSearchParams(search), [search])
}

export const ListContainer = withRouter(({ history }) => {
  const [criteria, setCriteria] = useState([])
  const [order, setOrder] = useState('default')
  const [unsortedEntries, setUnsortedEntries] = useState([])
  const [page, setPage] = useState(1)

  let pageParam = useQuery()

  let location = useLocation()

  useEffect(
    () => {
      const page = pageParam.get('page')
      setPage(page ? page : 1)
    },
    [location]
  )

  useEffect(() => {
    if(!unsortedEntries.length) {
      populateEntries()
    }
  }, [])

  const toggleOrder = (criteria) => {
    setCriteria(criteria)
    switch(order) {
      case 'asc':
        return setOrder('desc')
      case 'desc':
        return setOrder('default')
      case 'default':
        return setOrder('asc')
    }
  }

  async function populateEntries() {
    const entries = await getList()

    return setUnsortedEntries(transformCodesToBirthdays(entries))
  }

  const switchPage = (page) => {
    history.push({
      search: `?page=${ page }`
    })
  }

  let sortedEntries = unsortedEntries.length ? sortArr(unsortedEntries, criteria, order) : []
  const pagesAmount = Object.keys(divideArr(unsortedEntries)).length

  return (
    unsortedEntries.length ?
      <List
        criteria={ criteria }
        entries={ order === 'default' ? divideArr(unsortedEntries)[page] : divideArr(sortedEntries)[page] }
        order={ order }
        page={ page ? Number(page) : 1 }
        pagesAmount={ pagesAmount }
        setPage={ switchPage }
        toggleOrder={ toggleOrder }
      /> :
      'loading...'
  )
})
