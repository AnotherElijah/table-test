export const getList = async() => {
  let transformedList = []
  try {
    await fetch('http://proovitoo.twn.ee/api/list.json')
      .then((response) => {
        return response.json()
      })
      .then((data) => {
        const { list } = data
        transformedList = list
      })
  } catch(err) {
    console.log(err)
  }

  return transformedList
}