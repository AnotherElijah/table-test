import { MainRouter } from './routes'
import './styles.css'

function App() {
    return (
        <div className="App">
            <div className={ 'page-wrapper' }>
                <div className={ 'page-container' }>
                    <div className={ 'page-content' }>
                        <MainRouter/>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default App;
