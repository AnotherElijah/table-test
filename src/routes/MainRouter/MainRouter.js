import React from 'react'
import {
  BrowserRouter as Router,
  Link,
  Route,
  Switch
} from 'react-router-dom'
import { ListContainer } from '../../containers'

export const MainRouter = () => {
  return (
    <Router>
      <Switch>
        <Route path="/" exact>
          <>This is the main page. Please proceed to <Link to={ '/list' }>list</Link></>
        </Route>
        <Route path="/list">
          <ListContainer/>
        </Route>
      </Switch>
    </Router>
  )
}
