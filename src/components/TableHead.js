import React, { useState } from 'react'

export const TableHead = ({
                            criteria,
                            order,
                            toggleOrder
                          }) => {

  const headers = [
    { firstname: 'EESNIMI' },
    { surname: 'PEREKONNANIMI' },
    { sex: 'SUGU' },
    { birthday: 'SÜNNIKUUPÄEV' },
    { phone: 'TELEFON' }
  ]

  const renderArrows = (isActive) => {

    const defaultSort = <>
      <span>&#9650;</span>
      <span>&#9660;</span>
    </>

    const ascSort = <>
      <span style={ { visibility: 'hidden' } }>&#9650;</span>
      <span>&#9660;</span>
    </>

    const descSort = <>
      <span>&#9650;</span>
      <span style={ { visibility: 'hidden' } }>&#9660;</span>
    </>

    if(!isActive) return defaultSort
    switch(order) {
      case 'asc':
        return ascSort
      case 'desc':
        return descSort
      case 'default':
        return defaultSort
    }
  }

  const renderHeaders = () => {
    return headers.map((name, i) => {
        const key = Object.keys(name)[0]
        const locale = Object.values(name)[0]

        return <th key={ i } onClick={ () => toggleOrder(key) }>
          <div className={ 'thead-wrapper' }>
            <div className={ 'thead-name' }>{ locale }</div>
            <div className={ 'thead-arr' }>{ renderArrows(key === criteria) }</div>
          </div>
        </th>
      }
    )
  }

  return <tr>
    { renderHeaders() }
  </tr>
}