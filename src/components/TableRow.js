import React, { useState } from 'react'

export const TableRow = (
  { data }
) => {
  const [isActive, setIsActive] = useState(false)

  let intro = data.intro.replace('<p>', '').replace('</p>', '')

  return (
    <>
      <tr
        className={ isActive ? 'active clickable' : 'clickable' }
        onClick={ () => setIsActive(!isActive) }>
        <td>{ data.firstname }</td>
        <td>{ data.surname }</td>
        <td>{ data.sex }</td>
        <td>{ data.birthday }</td>
        <td>{ data.phone }</td>
      </tr>
      {
        isActive &&
        <tr>
          <td colspan={ '10' }>
            <div className={ 'person_info' }>
              <div className={ 'image' }>
                <img src={ data.image.small } alt={ data.image.title }/>
              </div>
              <div className={ 'content' }>
                <p>{ intro }</p>
              </div>
            </div>
          </td>
        </tr>
      }
    </>
  )
}