import React from 'react'
import { TableRow } from './TableRow'
import { TableHead } from './TableHead'
import { Pagination } from './Pagination'

export const List = ({
                     criteria,
                     entries,
                     order,
                     page,
                     pagesAmount,
                     setPage,
                     toggleOrder
                     }) => {
  return (
    <div>
      {
        (<table>
          <thead>
          <TableHead toggleOrder={ toggleOrder } criteria={ criteria } order={ order }/>
          </thead>
          <tbody>
          { entries.map(entry => <TableRow key={ entry.id } data={ entry }/>) }
          </tbody>
        </table>)
      }
      <Pagination setPage={ setPage } page={ page } pagesAmount={ pagesAmount }/>
    </div>
  )
}
