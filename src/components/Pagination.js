import React from 'react'

export const Pagination = ({
                             setPage,
                             page,
                             pagesAmount
                           }) => {

  const renderButtons = () => {

    let buttons = []

    buttons.push(<button key={ 11 } onClick={ () => page > 1 && setPage(--page) }>&#8249;</button>)

    for(let i = 1; i <= pagesAmount; i++) {

      if(
        page > 3 &&
        i <= (pagesAmount - 5) &&
        (page - 2) > i) {
        continue
      }

      if(
        page < 8 &&
        i >= (6) &&
        i > (page + 2)) {
        continue
      }

      buttons.push(
        <button
          key={ i }
          className={ i === page ? 'active' : '' }
          onClick={ () => setPage(i) }>
          { i }
        </button>
      )
    }

    buttons.push(<button key={ 12 } onClick={ () => page < 10 && setPage(++page) }>&#8250;</button>)

    return buttons
  }
  return <div className="pagination">
    { renderButtons() }
  </div>
}