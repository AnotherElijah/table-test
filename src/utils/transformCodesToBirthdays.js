import { transformToBirthDate } from './transformPersonalCode'

export const transformCodesToBirthdays = (entries) => {
  return entries.map(entry => ({
    ...entry,
    birthday: transformToBirthDate(String(entry.personal_code))
  }))
}
