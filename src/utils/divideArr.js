export const divideArr = (arr, step = 10) => {
  let arrCopy = JSON.parse(JSON.stringify(arr))
  let page = 1
  let chunks = {}

  while(arrCopy.length) {
    const chunk = arrCopy.splice(0, step)
    chunks[page] = chunk
    page++
  }

  return chunks
}