export const sortArr = (entries,
                        criteria,
                        order) => {

  if(order === 'default') return entries

  let listCopy = JSON.parse(JSON.stringify(entries))

  return listCopy.sort(function(a, b) {
    if(a[criteria] > b[criteria]) {
      return order === 'asc' ? 1 : -1
    }
    if(a[criteria] < b[criteria]) {
      return order === 'asc' ? -1 : 1
    }
    return 0
  })
}

//https://medium.com/coding-at-dawn/sorts-in-60-seconds-speedy-javascript-interview-answers-on-sorting-acb72bdea8a2#:~:text=%E2%80%9CQuicksort%20it%20is%20probably%20the,flexible%20on%20it's%20input%20data.