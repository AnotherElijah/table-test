export const transformToBirthDate = (code) => {
  const century = code.substr(0, 1)
  let yearA = ''
  const yearB = code.substr(1, 2)
  const month = code.substr(3, 2)
  const day = code.substr(5, 2)

  const a = ['1', '2'].includes(century)
  const b = ['3', '4'].includes(century)
  const c = ['5', '6'].includes(century)
  const d = ['7', '8'].includes(century)

  if(a) yearA = '18'
  if(b) yearA = '19'
  if(c) yearA = '20'
  if(d) yearA = '21'

  return `${ yearA }${ yearB }.${ month }.${ day }`
}